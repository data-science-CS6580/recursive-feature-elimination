from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.metrics import accuracy_score
from sklearn.feature_selection import RFE
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.tree import DecisionTreeClassifier


def MLcomparison(x_train,x_test,y_train,y_test):
    #Random Forest Classifier
    print("\nRandom Forest Classifier")
    rfc = RandomForestClassifier(max_depth=2, random_state=0)
    rfc.fit(x_train, y_train)
    print(f"accuracy = {rfc.score(x_test, y_test)}")

    #Decision Tree Classifier
    print("\nDecision Tree Classifier")
    dtc = DecisionTreeClassifier(random_state=0)
    dtc.fit(x_train, y_train)
    print(f"accuracy = {dtc.score(x_test, y_test)}")

    #SVC
    print("\nSVC")
    svc = make_pipeline(StandardScaler(), SVC(gamma='auto'))
    svc.fit(x_train, y_train)
    print(f"accuracy = {svc.score(x_test, y_test)}")

    #Gradient Boosting Classifier
    print("\nGradient Boosting Classifier")
    gbc = GradientBoostingClassifier(n_estimators=100, learning_rate=1.0, max_depth=1, random_state=0).fit(x_train, y_train)
    print(f"accuracy = {gbc.score(x_test, y_test)}")

    #Logistic Regression
    print("\nLogistic Regression")
    lm = LogisticRegression(random_state=0).fit(x_train,y_train)
    print(f"accuracy = {lm.score(x_test, y_test)}")



if __name__ == "__main__":


    df = pd.read_csv('banking.csv')
    features = np.array(['age','job','marital','education','default','housing','loan','contact','month','day_of_week','duration','campaign','pdays','previous','poutcome','emp_var_rate','cons_price_idx','cons_conf_idx','euribor3m','nr_employed'])
    df = df.apply(LabelEncoder().fit_transform)

    #data in X and ansers in y
    X = df.loc[:, features]
    y = df.iloc[:, len(features)]
    print(f"X = {X}")
    print(f"y = {y}")

    #saving 20% for testing, 80% for training 
    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    classifier = LogisticRegression()
    classifier.fit(x_train, y_train)
    y_pred = classifier.predict(x_test)

    cm = confusion_matrix(y_test, y_pred)
    print(f'Accuracy = {str(accuracy_score(y_test, y_pred))}')

    #each iteration removes one feature
    goodFeatures = []
    accuracies = []
    for num_components in range(19,0,-1):
        print(f'\n\nShowing RFE with only {num_components} features.')
        model = LogisticRegression(max_iter=855000)

        rfe = RFE(model, n_features_to_select=num_components)
        X = df.loc[:, features]
        rfe = rfe.fit(X, y)

        new_features = features[rfe.support_]
        goodFeatures.append(new_features)
        print(f"new_features={new_features}")


        X = df.loc[:, new_features]
        # y stays the same
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
        classifier.fit(X_train, y_train)
        y_pred = classifier.predict(X_test)
        cm = confusion_matrix(y_test, y_pred)
        print(cm)
        print(f'Accuracy = {str(accuracy_score(y_test, y_pred))}')
        accuracies.append(accuracy_score(y_test, y_pred))

    #print out num features vs accuracy in a easier to read format 
    for i in range(0, len(goodFeatures)-1):
        print(str(goodFeatures[i]) + " \n" + str(accuracies[i]))
    for xx in accuracies:
        print(xx)

    MLcomparison(X_train, X_test, y_train, y_test)

    #running the algorithems again with the best 5 features found above
    print("\n\nrunning again with only 5 Features")
    df = df.drop(['age','job','marital','month','education','housing','loan','day_of_week','duration','campaign','pdays','emp_var_rate','cons_price_idx','cons_conf_idx','euribor3m'], axis=1)
    newFeatures = ['default', 'contact', 'previous', 'poutcome', 'nr_employed']
    X = df.loc[:, newFeatures]
    y = df.iloc[:, len(newFeatures)]

    
    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    MLcomparison(x_train, x_test, y_train, y_test)
    print(f"X = {X}")
    print(f"y = {y}")