# Recursive Feature Elimination

Using the data set in the banking.csv file figure out which of the values best help determine success. 

## Execute Project 
```bash
python3 rfe.py
```